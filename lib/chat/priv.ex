defmodule Chat.Priv do
  use Reactive.Entity
  require Logger

  def init(args=[user1o,user1id,user2o,user2id]) do
    priv=[Chat.Priv | args]
    messages=[Chat.Messages | priv]

    Reactive.Entity.request(messages,:create)

    Logger.info("Priv created #{inspect args}")

    save_me()

    {:ok,%{
      id: priv,
      messages: messages,
      user1: [Chat.User,user1o,user1id],
      user2: [Chat.User,user2o,user2id],
      info: %{}
    },%{}}
  end

  def request(:create ,state,_from,_rid) do
    {:reply,:ok,state}
  end

  def get(:users,state) do
    {:reply,{state.user1,state.user2},state}
  end

  def get_priv([Chat.User,o1,i1]=user1,[Chat.User,o2,i2]=user2) do
    if(user1<user2) do
      [Chat.Priv,o1,i1,o2,i2]
    else
      [Chat.Priv,o2,i2,o1,i1]
    end
  end
  
end