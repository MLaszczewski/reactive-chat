defmodule Chat.User do
  use Reactive.Entity
  require Logger

  @close_conversations_when_offline Application.get_env(:chat, :close_conversations_when_offline, false)

  def init([ownerType,ownerId]) do
    {:ok,%{
      owner_type: ownerType,
      owner_id: ownerId,
      id: [Chat.User,ownerType,ownerId],
      conversations: [],
      read_positions: %{},
      invitations: [],
      online_setting: true,
      focus: :none,
      typing: false,
      connection_status: :disconnected,
      identity: %{
        type: :anonymous,
        name: "Anon"<>Integer.to_string(rem(Reactive.Entity.timestamp(),10000))
      }
    },%{
    }}
  end

  def retrive(id) do
    r=Reactive.Entities.retrive_entity(id)
    case r do
      :not_found -> :not_found
      {:ok,%{state: state, container: container}} ->
        notify_observers(:connection_status, {:set,[:disconnected]})
        notify_observers(:online_status, {:set,[false]})
        nstate=if(@close_conversations_when_offline) do
          nst=%{ state | conversations: [], read_positions: %{} }
          notify_observers(:conversations,{:set,[[]]})
          notify_observers(:read_positions,{:set,[[]]})
          Enum.each(state.conversations,fn(conversation) ->
            Reactive.Entity.event([Chat.UsersList | conversation],{:remove_user,state.id})
          end)
          save_me()
          nst
        else
          state
        end
        {:ok,%{state: nstate, container: container}}
    end
  end

  def observers(:online,state,[_],[]) do
    notify_observers(:connection_status, {:set,[:disconnected]})
    notify_observers(:online_status, {:set,[false]})
    notify_observers(:typing, {:set,[false]})
    if(@close_chat_conversations_when_offline) do
      nstate=%{ state | conversations: [], read_positions: %{} }
      notify_observers(:conversations,{:set,[[]]})
      notify_observers(:read_positions,{:set,[[]]})
      Enum.each(state.conversations,fn(conversation) ->
        Reactive.Entity.event([Chat.UsersList | conversation],{:remove_user,state.id})
      end)
      save_me()
    end
    %{state | connection_status: :disconnected }
  end
  def observers(:online,state,[],[_]) do 
    notify_observers(:connection_status, {:set,[:connected]})
    notify_observers(:online_status, {:set,[state.online_setting]})
    %{state | connection_status: :connected }
  end
  def observers(_,state,_,_), do: state

  def get(:online,state) do
    {:reply,true,state}
  end
  def get(:conversations,state) do
    {:reply,state.conversations,state}
  end
  def get(:read_positions,state) do
    {:reply,Enum.map(state.read_positions,fn({c,p}) -> %{conversation: c, position: p} end),state}
  end
#  def get(:conversations_info,state) do
#    {:reply,Enum.map(state.conversations,fn(c) -> Reactive.Entity.get(c,:info) end),state}
#  end
  def get(:invitations,state) do
    {:reply,state.invitations,state}
  end
  def get(:connection_status,state) do
    {:reply,state.connection_status,state}
  end
  def get(:identity,state) do
    {:reply,state.identity,state}
  end
  def get(:online_setting,state) do
    {:reply,state.online_setting,state}
  end
  def get(:online_status,state) do
    {:reply,state.online_setting && (state.connection_status==:connected),state}
  end
  def get(:focus,state) do
    {:reply,state.focus,state}
  end
  def get(:typing,state) do
    {:reply,state.typing,state}
  end

  def request({:set_identity,identity},state,_from,_rid) do
    Logger.debug("CHAT USER set identity #{inspect identity}")
    nstate=%{state | identity: identity}
    notify_observers(:identity,{:set,[identity]})
    save_me()
    {:reply,:ok,nstate}
  end
  def request({:set_online,os},state,_from,_rid) do
    nstate=%{state | online_setting: os}
    status=(state.online_status== :connected) && state.online_setting
    notify_observers(:online_setting,{:set,[os]})
    notify_observers(:online_status,{:set,[status]})
    {:reply,:ok,nstate}
  end
   def request({:set_typing,ws},state,_from,_rid) do
    nstate=%{state | typing: ws}
    notify_observers(:typing,{:set,[ws]})
    {:reply,:ok,nstate}
  end
  def request({:open_conversation,conversation},state,_from,_rid) do
    #Logger.debug("ADD CONV #{inspect state.conversations} , #{inspect conversation}")
    count=Enum.count(state.conversations,fn(x) -> x==conversation end)
    if(count>0) do
      {:reply,:ok,state}
    else
      nstate=%{ state | conversations: [conversation | state.conversations] }
      notify_observers(:conversations,{:push,[conversation]})
      ul = [Chat.UsersList | conversation]
      Reactive.Entity.event(ul,{:add_user,state.id})
      save_me()
    #  notify_observers(:conversations_info,{:push,[Reactive.Entity.get(conversation,:conversation_info)]})
      {:reply,:ok,nstate}
    end
  end
  def request({:close_conversation,conversation},state,_from,_rid) do
    #Logger.debug("RM CONV #{inspect state.conversations} , #{inspect conversation}")
    count=Enum.count(state.conversations,fn(x) -> x==conversation end)
    if(count==0) do
      {:reply,:ok,state}
    else
      nstate=%{ state | conversations: Enum.filter(state.conversations,fn(c) -> c != conversation end),
        read_positions: Map.delete(state.read_positions,conversation) }
      notify_observers(:conversations,{:remove,[conversation]})
      ul = [Chat.UsersList | conversation]
      notify_observers(:read_positions,{:removeBy,[:conversation,conversation]})
      Reactive.Entity.event(ul,{:remove_user,state.id})
      save_me()
    #  notify_observers(:conversations_info,{:removeBy,[:id,conversation]})
      {:reply,:ok,nstate}
    end
  end
  def request({:set_focus,conversation},state,_from,_rid) do
    if state.focus != :none do
      Entity.event(state.focus,{:not_focused,state.id})
    end
    nstate=%{ state | focus: conversation }
    notify_observers(:focus, conversation)
    Entity.event(nstate.focus,{:focused,state.id})
    {:reply,:ok,nstate}
  end
  def request({:add_invitation,invitation},state,_from,_rid) do
    nstate=%{ state | invitations: [invitation | state.invitations] }
    notify_observers(:invitations,{:push,[invitation]})
    {:reply,:ok,nstate}
  end
  def request({:ignore_invitation,invitation_id},state,_from,_rid) do
    {[invitation],ninvitations}=Enum.partition(state.invitations,fn(inv) ->
      inv.id==invitation_id
    end)
    r=Reactive.Entity.request(invitation.entity,{:ignore, state.id})
    nstate=%{ state | invitations: ninvitations }
    notify_observers(:invitations,{:removeBy,[:id,invitation_id]})
    {:reply,r,nstate}
  end
  def request({:accept_invitation,invitation_id},state,_from,_rid) do
    {[invitation],ninvitations}=Enum.partition(state.invitations,fn(inv) ->
      inv.id==invitation_id
    end)
    r=Reactive.Entity.request(invitation.entity,{:accept, state.id})
    nstate=%{ state | invitations: ninvitations }
    notify_observers(:invitations,{:removeBy,[:id,invitation_id]})
    {:reply,:ok,nstate}
  end
  def request({:cancel_invitation,invitation_id},state,_from,_rid) do
    {[invitation],ninvitations}=Enum.partition(state.invitations,fn(inv) ->
      inv.id==invitation_id
    end)
    nstate=%{ state | invitations: ninvitations }
    notify_observers(:invitations,{:removeBy,[:id,invitation_id]})
    {:reply,:ok,nstate}
  end

  def request({:say,conversation,text},state,_from,_rid) do
    r=push_message(conversation,state,%{
      type: :say,
      text: text
    })
    {:reply,r,state}
  end

  def request({:put_message,conversation,message},state,_from,_rid) do
    r=push_message(conversation,state,message)
    {:reply,r,state}
  end

  def request({:like,conversation,msg_id},state,_from,_rid) do
    r=Reactive.Entity.request([Chat.Messages | conversation],{:push_to_message_field,msg_id,:likes,state.owner_id})
    {:reply,r,state}
  end

  def request({:unlike,conversation,msg_id},state,_from,_rid) do
    r=Reactive.Entity.request([Chat.Messages | conversation],{:remove_from_message_field,msg_id,:likes,state.owner_id})
    {:reply,r,state}
  end

  def request({:set_read_position,conversation,position},state,_from,_rid) do
    nreadp = Map.put(state.read_positions,conversation,position)
    nstate = %{ state | read_positions: nreadp }
    notify_observers(:read_positions,{:putBy,[:conversation,conversation,%{ conversation: conversation, position: position}]})
    nstate
  end

 # def notify(conversation, :conversation_info , info, state) do
 #   notify_observers(:converasations_info, {:updateBy,[:id,info.id,info]})
 #   state
 # end

  def push_message(conversation,state,message) do
    messages=[Chat.Messages | conversation]
    Reactive.Entity.request(messages,{:push_message,Map.put(message,:who,state.identity)})
  end
end
