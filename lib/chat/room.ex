defmodule Chat.Room do
  use Reactive.Entity
  require Logger

  def init(args=[name]) do
    room=[Chat.Room | args]
    messages=[Chat.Messages | room]
    users=[Chat.UsersList | room]

    Reactive.Entity.request(messages,:create)
    Reactive.Entity.request(users,:create)

    Logger.info("Room created #{inspect room}")

    save_me()

    {:ok,%{
      id: room,
      messages: messages,
      users: users,
      name: name,
      info: %{}
    },%{}}
  end

  def request({:set_info,info} ,state,_from,_rid) do
    {:reply,:ok,Map.put(state,:info,info)}
  end

  def get(:info,state) do
    {:reply,state.info,state}
  end

  def get(:mode,state) do
    {:reply,state.mode,state}
  end
end