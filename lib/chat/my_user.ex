defmodule Chat.MyUser do
  use Reactive.SessionEntityFacade

  def entity_module do
    Chat.User
  end

  def observations do
    %{
      conversations: &forward_list/2,
 #     conversations_info: &forward_list/2,
      read_positions: &forward_list/2,
      online: &forward_value/2,
      identity: &forward_value/2,
      online_setting: &forward_value/2,
      invitations: &forward_list/2
    }
  end

  def request({:api_request,[:say,room,text],_contexts} ,state,_from,_rid) when is_binary(text) do
    if(:erlang.byte_size(text)>1014) do
      raise "too long text message - hacking attempt!"
    end
    r=Reactive.Entity.request(state.entity,{:say,room,text})
    {:reply,r,state}
  end
  def request({:api_request,[:set_online,os],_contexts} ,state,_from,_rid) when is_atom(os) do
    r=Reactive.Entity.request(state.entity,{:set_online,os})
    {:reply,r,state}
  end
  def request({:api_request,[:join,roomId],_contexts} ,state,_from,_rid) when is_binary(roomId) do
    room=[Chat.Room,roomId]
    r=Reactive.Entity.request(state.entity,{:open_conversation,room})
    {:reply,r,state}
  end
  def request({:api_request,[:leave,roomId],_contexts} ,state,_from,_rid) when is_binary(roomId) do
    room=[Chat.Room,roomId]
    r=Reactive.Entity.request(state.entity,{:close_conversation,room})
    {:reply,r,state}
  end
  def request({:api_request,[:like,room,msg_id],_contexts} ,state,_from,_rid) when is_binary(msg_id) do
    r=Reactive.Entity.request(state.entity,{:like,room,msg_id})
    {:reply,r,state}
  end
  def request({:api_request,[:unlike,room,msg_id],_contexts} ,state,_from,_rid) when is_binary(msg_id) do
    r=Reactive.Entity.request(state.entity,{:unlike,room,msg_id})
    {:reply,r,state}
  end
  def request(:get_user_id ,state,_from,_rid) do
    {:reply,state.entity,state}
  end

  def request({:api_request,[:open_priv,to],_contexts} ,state,_from,_rid) do
    priv=Chat.Priv.get_priv(state.entity,to)
    Reactive.Entity.request(priv,:create)
    Reactive.Entity.request(state.entity,{:open_conversation,priv})
    Reactive.Entity.request(to,{:open_conversation,priv})
    {:reply,priv,state}
  end
  def request({:api_request,[:close_priv,to],_contexts} ,state,_from,_rid) do
    priv=Chat.Priv.get_priv(state.entity,to)
    r=Reactive.Entity.request(state.entity,{:close_conversation,priv})
    {:reply,priv,state}
  end

end