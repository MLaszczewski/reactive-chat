defmodule Chat.UsersList do
  use Reactive.Entity
  require Logger

  @close_conversations_when_offline Application.get_env(:chat, :close_conversations_when_offline, false)

  def init(args) do
    Logger.info("Users list created #{inspect args}")
    save_me()
    {:ok,%{
      conversation: args,
      users: %{},
      users_count: 0
    },%{}}
  end

  def retrive(id) do
    r=Reactive.Entities.retrive_entity(id)
    case r do
      :not_found -> :not_found
      {:ok,%{state: state, container: container}} ->
        nst=Enum.reduce(Map.to_list(state.users),state, fn({user,info},st) ->
          online = Reactive.Entity.get(user, :online_status)
          if(online) do
            st
          else
            event({:remove_user,user},st,:undefined)
          end
        end)
        {:ok,%{state: nst, container: container}}
    end
  end

  def get(:users,state) do
  Logger.debug("GETUSERS! #{inspect state.users} > #{inspect state.conversation}")
    {:reply,Map.values(state.users),state}
  end

  def get(:users_count,state) do
    {:reply,state.users_count,state}
  end

  def request(:create ,state,_from,_rid) do
    {:reply,:ok,state}
  end

  def event({:add_user,user},state,from) do
    Logger.debug("ADDUSER! #{inspect user} > #{inspect state.conversation}")
    observe(user,:identity)
    observe(user,:online_status)
    observe(user,:focus)
    observe(user,:typing)
    info=%{ id: user }
    nusers=Map.put(state.users,user,info)
    notify_observers(:users,{:push,[info]})
    nusers_count=state.users_count+1
    notify_observers(:users_count,{:set,[nusers_count]})
    save_me()
    %{ state | users: nusers, users_count: nusers_count}
  end

  def event({:remove_user,user},state,from) do
    if(!Map.has_key?(state.users,user)) do
      state
    else
      unobserve(user,:identity)
      unobserve(user,:online_status)
      unobserve(user,:focus)
      unobserve(user,:typing)
      nusers=Map.delete(state.users,user)
      notify_observers(:users,{:removeBy,[:id,user]})
      nusers_count=state.users_count-1
      notify_observers(:users_count,{:set,[nusers_count]})
      save_me()
      %{ state | users: nusers, users_count: nusers_count}
    end
  end

  def notify(user=[Chat.User,_,_],what,{:set,[data]},state) do
    Logger.debug("Users List detected user change #{inspect what} #{inspect data} ")
    udata=Map.get(state.users,user,:none)
    nstate=if(udata==:none) do
      state
    else
      nudata=Map.put(udata,what,data)
      nusers=Map.put(state.users,user,nudata)
      notify_observers(:users,{:updateFieldBy,[:id,user,what,data]})
      save_me()
      %{ state | users: nusers}
    end
   # if(what == :online and data == :false and @close_conversations_when_offline) do
   #   event({:remove_user,user},nstate,:undefined)
   # else
      nstate
   # end
  end
end
