defmodule Chat.Messages do
  use Reactive.LogEntity
  require Logger

  def init(args) do
    id = [Chat.Messages | args]
    Logger.info("Messages pub/sub created #{inspect id}")

    save_me()
    {:ok,init_log(%{
      id: id,
      uniq: 0,
      ts: 0
    },id),%{}}
  end

  def observe(:messages,state,_pid) do
    {:reply,{:sync,[get_log_key(state.ts,Integer.to_string(state.uniq-1))]},state}
  end

  def request({:push_message,message} ,state,_from,_rid) do
    ts=Reactive.Entity.timestamp()
    key=add_to_log(state,ts,Integer.to_string(state.uniq),message)
    notify_observers(:messages,{:push,[Map.put(message, :id, key)]})
    save_me()
    {:reply,%{ type: :pushed, id: key },Map.put(Map.put(state, :uniq, state.uniq+1 ),:ts,ts)}
  end

  def request(:create ,state,_from,_rid) do
    add_to_log(state,Reactive.Entity.timestamp(),Integer.to_string(state.uniq),%{
      type: :created
    })
    {:reply,:ok,%{ state | uniq: state.uniq+1 }}
  end

  def update_message_field(state,key,field_name,update_fun) do
    nvalue=update_log(state,key,fn(data) ->
      nfd = update_fun.(Map.get(data,field_name))
      Map.put(data,field_name,nfd)
    end)
    notify_observers(:messages,{:updateFieldBy,[:id,key,field_name,Map.get(nvalue,field_name)]})
  end

  def request({:set_message_field,key,field_name,value}, state, _from, _rid) do
    update_message_field(state,key,field_name,fn(x) -> value end)
    {:reply,:ok,state}
  end

  def request({:push_to_message_field,key,field_name,value}, state, _from, _rid) do
    update_message_field(state,key,field_name,fn(x) ->
      if(x==:nil) do [value] else [value | x] end
    end)
    {:reply,:ok,state}
  end

  def request({:remove_from_message_field,key,field_name,value}, state, _from, _rid) do
    update_message_field(state,key,field_name,fn(x) ->
      if(x==:nil) do
        []
      else
        Enum.filter(x,fn(y)-> y != value end)
      end
    end)
    {:reply,:ok,state}
  end

# def request({:api_request,[:get_messages,scan],_contexts} ,state,_from,_rid) do
#
# end

  def api_request(:get_messages,serviceId,_contexts,from \\ :end,to \\ :begin,limit \\ 100, reverse \\ true) do
    f=case from do
      "@begin" -> :begin
      "@end" -> :end
      _ -> from
    end
    t=case to do
      "@begin" -> :begin
      "@end" -> :end
      _ -> to
    end
   # Logger.debug("LogsDb.scan(#{inspect log},#{inspect f},#{inspect t},#{inspect limit},#{inspect reverse})")
    msgs = raw_scan(%{log: serviceId}, f, t, limit, reverse)
    Enum.map(msgs,fn({k,msg}) -> Map.put(msg,:id,k) end)
  end
end