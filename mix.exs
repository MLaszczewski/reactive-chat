defmodule Chat.Mixfile do
  use Mix.Project

  def project do
    [app: :chat,
     version: "0.1.0",
     elixir: ">= 1.2.3",
     deps: deps]
  end

  # Configuration for the OTP application
  #
  # Type `mix help compile.app` for more information
  def application do
    [applications: [:logger, :cowboy],
     mod: {Chat, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # To depend on another app inside the umbrella:
  #
  #   {:myapp, in_umbrella: true}
  #
  # Type `mix help deps` for more examples and rmoptions
  defp deps do
    [
      { :reactive_api, git: "git@bitbucket.org:ScalableEngineering/reactive-api.git" },
      { :reactive_session, git: "git@bitbucket.org:ScalableEngineering/reactive-session.git" },
      { :reactive_entity, git: "git@bitbucket.org:ScalableEngineering/reactive-entity.git"}
    ]
  end
end
